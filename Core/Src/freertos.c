/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
char shared_buf[32] = "";

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId LowTaskHandle;
osThreadId MidTaskHandle;
osThreadId HighTaskHandle;
osMessageQId RxQueueHandle;
osTimerId myTimer01Handle;
osMutexId myMutex01Handle;
osMutexId myMutex02Handle;
osSemaphoreId myBinarySem01Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
void printf_ram(void);

/* USER CODE END FunctionPrototypes */

void LowTaskFunc(void const * argument);
void MidTaskFunc(void const * argument);
void HighTaskFunc(void const * argument);
void Callback01(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* GetTimerTaskMemory prototype (linked to static allocation support) */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );

/* Hook prototypes */
void vApplicationIdleHook(void);
void vApplicationTickHook(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);
void vApplicationDaemonTaskStartupHook(void);

/* USER CODE BEGIN 2 */
__weak void vApplicationIdleHook( void )
{
   /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
   to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
   task. It is essential that code added to this hook function never attempts
   to block in any way (for example, call xQueueReceive() with a block time
   specified, or call vTaskDelay()). If the application makes use of the
   vTaskDelete() API function (as this demo application does) then it is also
   important that vApplicationIdleHook() is permitted to return to its calling
   function, because it is the responsibility of the idle task to clean up
   memory allocated by the kernel to any task that has since been deleted. */
}
/* USER CODE END 2 */

/* USER CODE BEGIN 3 */
__weak void vApplicationTickHook( void )
{
   /* This function will be called by each tick interrupt if
   configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h. User code can be
   added here, but the tick hook is called from an interrupt context, so
   code must not attempt to block, and only the interrupt safe FreeRTOS API
   functions can be used (those that end in FromISR()). */
}
/* USER CODE END 3 */

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
  printf("ov %s\r\n", (char*)pcTaskName);

}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
__weak void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
   printf("alloc failed\r\n");

}
/* USER CODE END 5 */

/* USER CODE BEGIN DAEMON_TASK_STARTUP_HOOK */
void vApplicationDaemonTaskStartupHook(void)
{
}
/* USER CODE END DAEMON_TASK_STARTUP_HOOK */

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/* USER CODE BEGIN GET_TIMER_TASK_MEMORY */
static StaticTask_t xTimerTaskTCBBuffer;
static StackType_t xTimerStack[configTIMER_TASK_STACK_DEPTH];

void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize )
{
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCBBuffer;
  *ppxTimerTaskStackBuffer = &xTimerStack[0];
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
  /* place for user code */
}
/* USER CODE END GET_TIMER_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* definition and creation of myMutex01 */
  osMutexDef(myMutex01);
  myMutex01Handle = osMutexCreate(osMutex(myMutex01));

  /* definition and creation of myMutex02 */
  osMutexDef(myMutex02);
  myMutex02Handle = osMutexCreate(osMutex(myMutex02));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of myBinarySem01 */
  osSemaphoreDef(myBinarySem01);
  myBinarySem01Handle = osSemaphoreCreate(osSemaphore(myBinarySem01), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of myTimer01 */
  osTimerDef(myTimer01, Callback01);
  myTimer01Handle = osTimerCreate(osTimer(myTimer01), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  osTimerStart (myTimer01Handle, 1000);

  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of RxQueue */
  osMessageQDef(RxQueue, UART_RX_ITEM_NUM, uint32_t);
  RxQueueHandle = osMessageCreate(osMessageQ(RxQueue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of LowTask */
  osThreadDef(LowTask, LowTaskFunc, osPriorityLow, 0, 512);
  LowTaskHandle = osThreadCreate(osThread(LowTask), NULL);

  /* definition and creation of MidTask */
  osThreadDef(MidTask, MidTaskFunc, osPriorityNormal, 0, 512);
  MidTaskHandle = osThreadCreate(osThread(MidTask), NULL);

  /* definition and creation of HighTask */
  osThreadDef(HighTask, HighTaskFunc, osPriorityHigh, 0, 1024);
  HighTaskHandle = osThreadCreate(osThread(HighTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_LowTaskFunc */
/**
  * @brief  Function implementing the LowTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_LowTaskFunc */
void LowTaskFunc(void const * argument)
{
  /* USER CODE BEGIN LowTaskFunc */
  osEvent event;
  uart_rx_t *rx;

  /* Infinite loop */
  for(;;)
  {
	event = osMessageGet(RxQueueHandle, osWaitForever);
#if 1
    if(event.status == osEventMessage){
        printf("%p\r\n", event.value.p);
        rx = event.value.p;
        printf("l=%d\r\n", rx->len);
        printf("%s\r\n", rx->buf);
        if(osErrorParameter == osPoolFree (Rx1Pool_Id, rx)){
            printf("free err %p\r\n", rx);
        }
        printf_ram();


        osSemaphoreRelease (myBinarySem01Handle);
    }else {
        printf("n\r\n");
    }
#endif
  }
  /* USER CODE END LowTaskFunc */
}

/* USER CODE BEGIN Header_MidTaskFunc */
/**
* @brief Function implementing the MidTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_MidTaskFunc */
void MidTaskFunc(void const * argument)
{
  /* USER CODE BEGIN MidTaskFunc */
  int cnt=0;
  /* Infinite loop */
  for(;;)
  {
    if(osOK == osSemaphoreWait ( myBinarySem01Handle, osWaitForever)){
        
        // printf_ram();
    osMutexWait ( myMutex01Handle, osWaitForever);
//        strcpy(shared_buf, "mid");

      // int len = strlen(shared_buf);
      // int len2 = sizeof(shared_buf);
      // sprintf(shared_buf, "%s[%d][%d] cnt=%d\r\n", "mid", len, len2, cnt);
      // printf("%s", shared_buf);
	    cnt++;
      osMutexRelease ( myMutex01Handle);
    }
  }
  /* USER CODE END MidTaskFunc */
}

/* USER CODE BEGIN Header_HighTaskFunc */
/**
* @brief Function implementing the HighTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_HighTaskFunc */
void HighTaskFunc(void const * argument)
{
  /* USER CODE BEGIN HighTaskFunc */
  int cnt=0;

  /* Infinite loop */
  for(;;)
  {
    osMutexWait ( myMutex01Handle, osWaitForever);
//    strcpy(shared_buf, "low-");

//   int len = strlen(shared_buf);
//   int len2 = sizeof(shared_buf);
//   sprintf(shared_buf, "%s[%d][%d] cnt=%d\r\n", "low-", len, len2, cnt);
//   printf("%s", shared_buf);


    cnt++;
    osMutexRelease ( myMutex01Handle);
    osDelay(5000);
  }
  /* USER CODE END HighTaskFunc */
}

/* Callback01 function */
void Callback01(void const * argument)
{
  /* USER CODE BEGIN Callback01 */
  static uint32_t cnt;
  printf("timer test %d\r\n", cnt);
  cnt++;
  /* USER CODE END Callback01 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
#define	RAM_LEN	(48*1024)
void printf_ram(void)
{
	uint32_t *p = (uint32_t*)0x20000000;
	int pos = 0 ;
	for(int i =0 ; i < RAM_LEN; i+=4){
		if(i % 16 == 0)
			printf("\r\n%p ", p+pos);

		printf("0x%08lx ",(uint32_t)*(p+pos));
		pos++;
	}
}

void printf_mem(uint32_t addr, int len) {
	int pos = 0;
    uint32_t *p = (uint32_t*)addr;
    uint32_t data;
    uint32_t cnt=0;
    printf("addr=%#lx len=%x\n", addr, len);

	for (int i = 0; i < len; i += 4) {
		if (i % 16 == 0) {
			printf("\n%p ", (p + pos));
		}
        data = (uint32_t) *(p + pos);
        printf("%#lx ", data);
        if(0xffffffff == data){
            cnt++;
            if(cnt >= 0x10){
                cnt = 0;
                break;
            }
        }else{
            cnt = 0;
        }
		pos++;
	}
	printf("\n");
}
/* USER CODE END Application */

